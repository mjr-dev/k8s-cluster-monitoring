# Amazon EKS Cluster

In this guide, we will deploy an EKS cluster in AWS. Since this project was developed using [KodeKloud AWS sandbox](https://kodekloud.com/playgrounds/playground-aws) for testing, This cluster utilises an ***unmanaged*** node group, i.e. one we have to deploy and join manually.


## Prereqs
- [Terraform](https://developer.hashicorp.com/terraform/install) `>=1.7.5`
- [AWS](https://aws.amazon.com/) account
- [kubectl](https://kubernetes.io/docs/tasks/tools/) `>=1.29.2`

## Create ec2 key pair
```bash
ssh-keygen -t rsa -b 4096 -m PEM -f ~/.ssh/eks-demo-key-pair
```

## Provision the infrastructure

```bash
cd eks/
terraform init
terraform plan
terraform apply
```

This may take up to 10 minutes to complete. When it completes, you will see something similar to this at the end of all the output. You will need the value of `NodeInstanceRole` later.

```
Example Outputs:

NodeAutoScalingGroup = "demo-eks-stack-NodeGroup-UUJRINMIFPLO"
NodeInstanceRole = "arn:aws:iam::387779321901:role/demo-eks-node"
NodeSecurityGroup = "sg-003010e8d8f9f32bd"
```

## Set up access and join nodes

1.  Update KUBECONFIG context for `kubectl`

    ```bash
    aws eks update-kubeconfig --region $AWS_REGION --name demo-eks
    ```

1.  Join the worker nodes

    1. Create environment variable for the `NodeInstanceRole` output value 

        ```
        export NODE_INSTANCE_ROLE="<NodeInstanceRoleValue>"
        ```

    1.  Apply the updated ConfigMap to join the nodes

        ```bash
        envsubst < cluster/aws-auth-cm.yaml | kubectl apply -f -
        ```

        Wait 2-3 minutes for node join to complete, then

        ```bash
        kubectl get node -o wide
        ```

        You should see 3 worker nodes in ready state. Note that with EKS you do not see control plane nodes, as they are managed by AWS.

## Provision the monitoring stack

```bash
cd eks/monitoring-stack
terraform init
terraform plan
terraform apply
```