module "cluster" {
  source = "./cluster"
  subnets = ["subnet-0dc6aa720e17f7680","subnet-0f4f1dabc9a1c7c42","subnet-0350a9da647122e0d"]
  aws_region = "us-east-1"
}

output "ClusterName" {
  value = module.cluster.ClusterName
}

output "NodeInstanceRole" {
  value = module.cluster.NodeInstanceRole
}

output "NodeSecurityGroup" {
  value = module.cluster.NodeSecurityGroup
}

output "NodeAutoScalingGroup" {
  value = module.cluster.NodeAutoScalingGroup
}

output "RoleArn" {
  value = module.cluster.RoleArn
}