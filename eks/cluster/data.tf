# Get the default VPC details
data "aws_vpc" "default_vpc" {
  default = true
}

# Get the subnets to use for the cluster ti bind to and the autoscaling group
# to place nodes in.
data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default_vpc.id]
  }
  filter {
    name = "availability-zone"
    values = [
      "${var.aws_region}a",
      "${var.aws_region}b",
      "${var.aws_region}c"
    ]
  }
}

# Get AMI ID for latest recommended Amazon Linux 2 image
data "aws_ssm_parameter" "node_ami" {
  name = "/aws/service/eks/optimized-ami/1.24/amazon-linux-2/recommended/image_id"
}

# Amazon EKS cluster data
data "aws_eks_cluster" "demo_eks" {
  name = aws_eks_cluster.demo_eks.name
}


# EC2 instance type data
data "aws_ec2_instance_type" "selected" {
  instance_type = var.instance_type
}
