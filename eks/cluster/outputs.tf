output "ClusterName" {
  value = data.aws_eks_cluster.demo_eks.name
}

output "NodeInstanceRole" {
  value = aws_iam_role.node_instance_role.arn
}

output "NodeSecurityGroup" {
  value = aws_security_group.node_security_group.id
}

output "NodeAutoScalingGroup" {
  value = aws_cloudformation_stack.autoscaling_group.outputs["NodeAutoScalingGroup"]
}

output "RoleArn" {
  value = aws_iam_role.demo_eks.arn
}