##############################
# Input variables
##############################
variable "eks_cluster_name" {
  type          = string
  description   = "The name of the Amazon EKS cluster."
  default       = "demo-eks"
}

variable "aws_region" {
  type    = string
  default = "us-east-2"
}

variable "instance_type" {
  type          = string
  description   = "The EC2 instance type to use for the worker nodes."
  default       = "t3.micro"
}

variable "desired_capacity" {
  type          = number
  description   = "The desired number of nodes to create in the node group."
  default       = 3
}

variable "min_size" {
  type          = number
  description   = "The minimum number of nodes to create in the node group."
  default       = 1
}

variable "max_size" {
  type        = number
  description = "The maximum number of nodes to create in the node group."
  default       = 4
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnet IDs to launch nodes in. Subnets automatically determine which availability zones the node group will reside."
}

variable "cluster_role_name" {
  type        = string
  description = "Name of the cluster role"
  default     = "eksClusterRole"
}

variable "node_role_name" {
  type        = string
  description = "The name to be used for the self-managed node group. By default, the module will generate a unique name."
  default     = "eks-demo-node"
}

variable "key_name" {
  type        = string
  description = "The name of the EC2 key pair to configure on the nodes."
  default     = "demo_ec2_key"
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Tags to apply to all tag-able resources."
  default     = { "cluster": "demo-eks"}
}

variable "namespace" {
    type = string
    description = "The namespace where the monitoring stack resources will be installed."
    default = "monitoring-stack"
  
}
