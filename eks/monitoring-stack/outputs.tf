output "ReleaseName" {
  value = helm_release.eks_kube_prometheus_stack.name
}

output "ChartName" {
  value = helm_release.eks_kube_prometheus_stack.chart
}

output "ChartVersion" {
  value = helm_release.eks_kube_prometheus_stack.version
}

output "ChartNamespace" {
  value = helm_release.eks_kube_prometheus_stack.namespace
}
