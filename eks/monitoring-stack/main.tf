terraform {
  required_providers {
    helm = {
        source = "hashicorp/helm"
        version = ">= 2.12.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}

provider "helm" {
    kubernetes {
      config_path = "~/.kube/config"
    }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "eks_kube_prometheus_stack_namespace" {
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "eks_kube_prometheus_stack" {
    name = "${var.helm_release_name}"
    repository = "${var.repo_url}"
    chart = "${var.chart_name}"
    version = "${var.chart_version}"
    namespace = "${var.namespace}"
}

