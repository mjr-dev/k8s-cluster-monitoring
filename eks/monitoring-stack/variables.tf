##############################
# Kube-Prometheus helm chart variables
##############################
variable "repo_url" {
  type = string
  description = "The helm chart repo url."
  default = "https://prometheus-community.github.io/helm-charts"
}

variable "chart_name" {
    type = string
    description = "The name of the helm chart to be installed."
    default = "kube-prometheus-stack"
}

variable "chart_version" {
  type = string
  description = "The helm chart version number."
  default = "57.1.0"
}

variable "helm_release_name" {
  type          = string
  description   = "The release name of your installed helm chart."
  default       = "eks-kube-prometheus"
}

variable "namespace" {
    type = string
    description = "The namespace where the monitoring stack resources will be installed."
    default = "monitoring-stack"
  
}
