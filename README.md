# k8s-cluster-monitoring

Provisioning various cloud k8s clusters and setting them up with the Grafana/Thanos/Prometheus monitoring stack.

## Tools needed
- [Terraform](https://developer.hashicorp.com/terraform/install) `>=1.7.5`
- Cloud provider

## Cluster Provisioning
- [AKS](aks/)
- [EKS](eks/)
- GKE (work in progress)