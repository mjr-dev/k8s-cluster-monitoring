##############################
# Required variables
##############################
variable "aks_cluster_name" {
  type          = string
  description   = "The name of the Azure AKS cluster."
}

variable "azure_region" {
  type    = string
  description = "The region where the AKS cluster will be deployed."
}

variable "network_plugin" {
    type = string
    description = "The AKS network plugin configuration type."
}

variable "k8s_version" {
  type = string
  description = "The kubernetes version for AKS cluster."
}

variable "resource_group_name" {
    type = string
    description = "The resource group name associated with AKS cluster."
}

variable "resource_group_location" {
  type = string
  description = "The region where the resource group is located."
}

variable "node_count" {
  type = number
  description = "The total node count for the node pool."
}

variable "min_node_count" {
  type = number
  description = "The minimum number of nodes in the node pool."
}

variable "max_node_count" {
    type = number
    description = "The maximum number of nodes in the node pool."
}

variable "subscription_id" {
  type = string
  description = "The Azure subscription ID."
}




##############################
# Optional variables
##############################
variable "tags" {
  type = map(string)
  description = "List of tags associated with AKS cluster"
  default = {
    "environment" = "demo",
    "cluster" = "aks-demo"
  }
}

variable "node_vm_size" {
  type = string
  description = "The size of the K8S node VM."
  default = "Standard_DS2_v2"
}

variable "address_space" {
  type = list(string)
  description = "The CIDR block address range for the AKS cluster."
  default = ["10.0.0.0/16"]
}

variable "subnet1_prefix" {
  type = list(string)
  description = "The CIDR block range for the first subnet."
  default = [ "10.0.0.0/19" ]
}

variable "subnet2_prefix" {
  type = list(string)
  description = "The CIDR block range for the second subnet."
  default = [ "10.0.32.0/19" ]
}
