output "ClusterName" {
  value = azurerm_kubernetes_cluster.aks_demo_cluster.name
}

output "ClusterRegion" {
  value = azurerm_kubernetes_cluster.aks_demo_cluster.location
}

output "ResourceGroupName" {
  value = azurerm_kubernetes_cluster.aks_demo_cluster.resource_group_name
}

output "ResourceGroupRegion" {
  value = azurerm_resource_group.aks_demo_rg.location
}

output "NodePoolName" {
  value = azurerm_kubernetes_cluster.aks_demo_cluster.default_node_pool[0].name
}

output "NodePoolCount" {
  value = azurerm_kubernetes_cluster.aks_demo_cluster.default_node_pool[0].node_count
}

output "Identity" {
  value = azurerm_kubernetes_cluster.aks_demo_cluster.identity[0].identity_ids
}