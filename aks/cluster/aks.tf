resource "azurerm_resource_group" "aks_demo_rg" {
  name     = var.resource_group_name
  location = var.resource_group_location
}

resource "azurerm_virtual_network" "aks_demo_vnet" {
  name = "demo-vnet"
  address_space = var.address_space
  location = var.azure_region
  resource_group_name = azurerm_resource_group.aks_demo_rg.name
  tags = var.tags
}

resource "azurerm_subnet" "subnet1" {
  name = "subnet1"
  address_prefixes = var.subnet1_prefix
  resource_group_name = azurerm_resource_group.aks_demo_rg.name
  virtual_network_name = azurerm_virtual_network.aks_demo_vnet.name
}

resource "azurerm_subnet" "subnet2" {
  name = "subnet2"
  address_prefixes = var.subnet2_prefix
  resource_group_name = azurerm_resource_group.aks_demo_rg.name
  virtual_network_name = azurerm_virtual_network.aks_demo_vnet.name
}

resource "azurerm_user_assigned_identity" "base" {
  name                = "base"
  location            = azurerm_resource_group.aks_demo_rg.location
  resource_group_name = azurerm_resource_group.aks_demo_rg.name
}

resource "azurerm_role_assignment" "base" {
  scope                = azurerm_resource_group.aks_demo_rg.id
  role_definition_name = "Network Contributor"
  principal_id         = azurerm_user_assigned_identity.base.principal_id
}

resource "azurerm_kubernetes_cluster" "aks_demo_cluster" {
  name                             = var.aks_cluster_name
  location                         = azurerm_resource_group.aks_demo_rg.location
  resource_group_name              = azurerm_resource_group.aks_demo_rg.name
  dns_prefix                       = "aks-demo"            
  kubernetes_version               =  var.k8s_version
  automatic_channel_upgrade        = "stable"
  private_cluster_enabled          = true 
  azure_policy_enabled             = false
  http_application_routing_enabled = false
  node_resource_group              = "${var.resource_group_name}-${var.aks_cluster_name}"
  sku_tier                         = "Free" 
  
  default_node_pool {
    name                  = "default"
    node_count            = var.node_count
    min_count             = var.min_node_count
    max_count             = var.max_node_count
    vm_size               = var.node_vm_size
    vnet_subnet_id        = azurerm_subnet.subnet1.id
    orchestrator_version  = var.k8s_version
    type                  = "VirtualMachineScaleSets"
    enable_auto_scaling   = true
    
    node_labels = {
      role = "general"
    }
  }

  network_profile {
    network_plugin = var.network_plugin
    dns_service_ip = "10.0.64.10"
    service_cidr   = "10.0.64.0/19"
  }

  identity {
    type         = "UserAssigned"
    identity_ids = [azurerm_user_assigned_identity.base.id]
  }

  tags = var.tags

  lifecycle {
    ignore_changes = [default_node_pool[0].node_count]
  }

  depends_on = [
    azurerm_role_assignment.base
  ]

}