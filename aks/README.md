# Azure Kubernetes Service (AKS) Cluster Setup

## Log in to Azure CLI and Set account subscription ID
```bash
az login
az account list
az account set --subscription <id>
```
## Create tfvars file
```bash
aks_cluster_name = "<cluster_name_here>"
azure_region = "<cluster_region_here>"
network_plugin = "<network_plugin_here>"
k8s_version = "<k8s_version_here>"
resource_group_name = "<resource_group_name_here>"
resource_group_location = "<resource_group_location_here>"
node_count = <node_count_here>
min_node_count = <min_node_count_here>
max_node_count = <max_node_account_here>
subscription_id = "<az_subscription_id_here>"
```

## Create AKS cluster using Terraform
```bash
terraform init
terraform plan -var-file .tfvars
terraform apply -var-file .tfvars
```

## Update KUBECONFIG context for `kubectl`
```bash
az aks get-credentials --resource-group <resource_group_name> --name <cluster_name>
```

## Check nodes after provisioning
```bash
kubectl get node -o wide
```